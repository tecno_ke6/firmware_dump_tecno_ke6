#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_TECNO-KE6.mk

COMMON_LUNCH_CHOICES := \
    lineage_TECNO-KE6-user \
    lineage_TECNO-KE6-userdebug \
    lineage_TECNO-KE6-eng
