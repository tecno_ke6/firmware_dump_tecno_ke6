#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/bootdevice/by-name/recovery:40894464:a38720b73682409a9ff926cdec5d88491b0a80e8; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/bootdevice/by-name/boot:33554432:056e07780cbe950a3baeae6fcf355f0ec98f7fb9 \
          --target EMMC:/dev/block/platform/bootdevice/by-name/recovery:40894464:a38720b73682409a9ff926cdec5d88491b0a80e8 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
